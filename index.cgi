#!/usr/bin/python

import os;

def showpage(title, content, headers = None, xmlmode = False):
   if xmlmode:
      print "Content-type: application/xhtml+xml"
   else:
      print "Content-type: text/html"
   if headers:
      for key in headers.keys():
         print "%s: %s" % (key, headers[key])
   print "" # content after here

   if xmlmode:
      print '<?xml version="1.0" encoding="utf-8"?>'

   print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">'
   print '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">'
   print '<head>'
   print '<title>%s</title>' % title
   print '<link rel="stylesheet" type="text/css" href="/examples/style.css" />'
   print '</head>'
   print '<body>'
   print '<div id="logo"><img src="http://www.debian.org/logos/openlogo-nd-50.png" alt="Swirl" />'
   print '<img src="http://www.debian.org/Pics/debian.jpg" alt="Debian" /></div>'
   print content
   print '</body>'
   print '</html>'


def readexample(path):
   try:
      content = ""

# Example name = name.txt
      fp = open("%s/name.txt" % path)
      content = "<h1>%s</h1>\n" % fp.read()
      fp.close()
# Example description = description.html

      fp = open("%s/description.html" % path)
      content = "%s%s\n" % (content, fp.read())
      fp.close()

# Example content = debian/*

      content = "%s<hr/>\n" % content
      content = "%s<h3>Debian directory contents</h3>\n" % content

      for root, dirs, files in os.walk("%s/debian" % path):
         for f in files:
            content = "%s<h4>%s/%s</h4>\n" % (content, root, f)
            fp = open("%s/%s" % (root, f))
            data = fp.read()
            data = data.replace("&", "&amp;")
            data = data.replace("<", "&lt;")
            data = data.replace(">", "&gt;")
            content = "%s<pre>%s</pre>\n" % (content, data)
            fp.close()

      return content
   except:
      return "<h1>Error in reading example %s</h1>" % path

def getexamples():
   content = "<h1>Example Packages</h1>\n"
   for entry in os.listdir("."):
      try:
         if os.path.exists("%s/debian" % entry):
            fp = open("%s/name.txt" % entry)
            content="%s<h2>%s</h2>\n" % (content, fp.read())
            fp.close()
            fp = open("%s/description.html" % entry)
            content="%s%s\n" % (content, fp.read())
            fp.close()
            content="%s<p>See the contents of this example: <a href='%s'>Debian dir</a> (<a href='tar.cgi/%s.tar.gz'>download</a>).</p>" % (content, entry, entry)
      except:
         pass
   return content
   
if os.environ.has_key("HTTP_ACCEPT") and os.environ["HTTP_ACCEPT"].find("application/xhtml+xml") >= 0:
   xmlmode = True
else:
   xmlmode = False

headers = None

if os.environ.has_key("REQUEST_URI"):

   path = os.environ["REQUEST_URI"].replace("/examples/", "").rstrip("/")
   if path.find("/") >= 0:
      headers = {"Status" : "401 Not Authorized"}
      title = "Invalid Request"
      content="<h1>Invalid Request</h1>"
   else:
      if path == "":
         fp = open("default.html")
         title = "Debian Java packaging examples"
         content = fp.read()
         content = "%s\n%s" % (content, getexamples())
         fp.close()
      elif os.path.exists("%s/debian" % path):
         title=path
         content=readexample(path)
      else:
         headers = {"Status" : "404 Not Found"}
         title="404 Not Found"
         content="<h1>404 Not Found</h1>"

else:
   fp = open("default.html")
   title = "Debian Java packaging examples"
   content = fp.read()
   content = "%s\n%s" % (content, getexamples())
   fp.close()

showpage(title, content, headers = headers, xmlmode = xmlmode)


